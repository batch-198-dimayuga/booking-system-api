const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: [true,"Firstname is required"]
    },
    lastName: {
        type: String,
        required: [true, "Lastname is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile Number is required"]
    },
    enrollments: 
    [
        {
            courseId: {
                type: String,
                required: [true, "Course Id is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Enrolled"
            }
        }
    ]
})

module.exports = mongoose.model("User",userSchema);

// user
// id,
// firstName - string,
// lastName - string,
// email - string,
// password - string,
// mobileNumber - string,
// isAdmin - boolean,
//             default: false
// enrollments - [
//     {
//         id,
//         courseId,
//         status - string,
//         dateEnrolled - date
//     }
// ]